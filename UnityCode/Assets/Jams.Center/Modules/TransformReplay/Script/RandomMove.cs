﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMove : MonoBehaviour {

    public Transform m_affected;
    public float m_minTimeChange=0.5f, m_maxTimeChange=3;
    public float m_moveSpeed = 1;

    

	IEnumerator Start () {

        while (true) {

            yield return new WaitForSeconds(Random.Range(m_minTimeChange, m_maxTimeChange));
            m_affected.rotation = Quaternion.Euler( new Vector3( Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f) ));
        }

	}
	
	void Update () {
         m_affected.Translate(Vector3.forward * m_moveSpeed * Time.deltaTime);
        
    }

    private void Reset()
    {
        m_affected = this.transform;
    }
    private void OnValidate()
    {
        if(m_affected==null)
        m_affected = this.transform;
    }
}
